//==================================================
// goal.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "goal.h"

#include "model_data.h"
#include "fade.h"
#include "game.h"
#include "manager.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGoal::CGoal(int nPriority) : CBlock(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGoal::~CGoal()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGoal::Init()
{
	CBlock::Init();

	CObjectX::SetModelData(CModelData::MODEL_GOAL);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGoal::Uninit()
{
	CBlock::Uninit();
}


//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGoal::Update()
{
	CBlock::Update();

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CGoal::Draw(DRAW_MODE drawMode)
{

	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	CBlock::Draw(drawMode);
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGoal *CGoal::Create(D3DXVECTOR3 pos, int Model)
{
	CGoal *pGoal;
	pGoal = new CGoal;

	if (pGoal != nullptr)
	{
		pGoal->Init();
		pGoal->SetPos(pos);
		pGoal->SetPosOriginPos(pos);
		pGoal->SetModelType(Model);
	}
	else
	{
		assert(false);
	}

	return pGoal;
}


//=============================================================================
// ヒット
//=============================================================================
void CGoal::hitevent()
{
	//CFade::GetInstance()->SetFade(CManager::MODE_GAME);
}