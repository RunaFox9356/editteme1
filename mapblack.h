//============================
//
// マップチップヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MAPBLACK_H_
#define _MAPBLACK_H_

//**************************************************
// インクルード
//**************************************************
#include "object2D.h"

//**************************************************
// クラス
//**************************************************
class CMapBlack : public CObject2D
{

public:
	
	explicit CMapBlack(int nPriority = PRIORITY_FADE);
	~CMapBlack();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CMapBlack *CMapBlack::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int nPriority);

	
private:

};

#endif	