//==================================================
// bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _BULLET_H_
#define _BULLET_H_

//**************************************************
// インクルード
//**************************************************
#include "object3d.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CBullet : public CObject3D
{
public:
	explicit CBullet(int nPriority = PRIORITY_BULLET);
	~CBullet();

	HRESULT Init() override;
	void Update() override;

	static CBullet *Create(D3DXVECTOR3 pos, D3DXVECTOR3 size);
private:
	void MoveBullet_();
private:
};

#endif	// _BULLET_H_