//**************************************************
//
// Hackathon ( imgui_property.cpp )
// Author  : katsuki mizuki
// Author  : Tanaka Kouta
// Author  : Hamada Ryuga
// Author  : Yuda Kaito
//
//**************************************************

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "stage_imgui.h"
//-----------------------------------------------------------------------------
// imgui
//-----------------------------------------------------------------------------
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui_internal.h"
#include <implot.h>

//-----------------------------------------------------------------------------
// json
//-----------------------------------------------------------------------------
#include <fstream>

//-----------------------------------------------------------------------------
// stage
//-----------------------------------------------------------------------------
#include "main.h"
#include "game.h"

#include "manager.h"
#include "texture.h"

#include "title.h"

#include "object.h"

#include "main.h"

#include "game.h"

#include "camera.h"
#include "map.h"

//-----------------------------------------------------------------------------
// コンストラクタ
//-----------------------------------------------------------------------------
CStageImgui::CStageImgui() :
	m_sliderIntmodel(0),
	m_sliderIntmesh(0)
{
}

//-----------------------------------------------------------------------------
// デストラクタ
//-----------------------------------------------------------------------------
CStageImgui::~CStageImgui()
{
}

//-----------------------------------------------------------------------------
// 初期化
//-----------------------------------------------------------------------------
HWND CStageImgui::Init(HWND hWnd, LPDIRECT3DDEVICE9 pDevice)
{
	// 初期化
	HWND outWnd = CImguiProperty::Init(hWnd, pDevice);

	// 鉄球データを初期化
	m_Pendulum.type = 0;
	m_Pendulum.coefficient = 0.0f;
	m_Pendulum.destRot.x = 0.0f;
	m_Pendulum.destRot.y = 0.0f;
	m_Pendulum.destRot.z = 0.0f;

	// ボールデータを初期化
	m_Ball.rotType = 0;
	m_Ball.radius = 0.0f;
	m_Ball.rotSpeed = 0.0f;

	// 丸太データを初期化
	m_Wood.coolTime = 0;
	m_Wood.rot = 0.0f;
	m_Wood.rotSpeed = 0.0f;
	m_Wood.move = 0.0f;
	for (int i = 0; i < D3DXyz; i++)
	{
		m_sliderRot3[i] = 0.0f;
		m_sliderNowRot3[i] = 0.0f;
		m_PosVSet[i] = 0.0f;
		m_PosRSet[i] = 0.0f;
		m_posRDest[i] = 10.0f;
		m_posVDest[i] = 0.0f;
		m_fDistance[i] = 1.0f;
	}
	m_posVDest[1] = D3DX_PI;

	m_BossAnimation = 0;
	m_KeySet = 0;
	m_indexModel = 0;
	m_indexNouEnemy = 0;
	m_block = 0;
	return outWnd;
}

//-----------------------------------------------------------------------------
// 終了
//-----------------------------------------------------------------------------
void CStageImgui::Uninit(HWND hWnd, WNDCLASSEX wcex)
{
	CImguiProperty::Uninit(hWnd, wcex);
}

//-----------------------------------------------------------------------------
// 更新
//-----------------------------------------------------------------------------
bool CStageImgui::Update()
{

	m_block = 0;
	CImguiProperty::Update();
	CMap* Map = CGame::GetMap();

	if (ImGui::CollapsingHeader(u8"座標設定"))
	{
		ImGui::Text("Log:%f", Map->GetLog());
		for (int block = 0; block < Map->GetblockSize(); block++)
		{
			//ImGui::ListBox("listbox", &item_current, items, IM_ARRAYSIZE(items), 4);
			CBlock *Modelblock = Map->Getblock(block);

			ImguiblockPos(Modelblock->GetPos(), Modelblock->GetMovePos(), Modelblock->GetNouPos(), Modelblock->GetModelType());
			ImGui::Separator();
		}
	}
	// テキスト表示
	ImGui::Text("FPS  : %.2f", ImGui::GetIO().Framerate);

	if (ImGui::Button("Save"))
	{
		CGame::GetMap()->Save();
	}
	
	if (ImGui::Button("Load"))
	{
		CGame::GetMap()->Load();
	}

	// 整数のスライド
	ImGui::SliderInt(u8"発生モデルのタイプ", &m_sliderIntmodel, 0, CMap::BLOCKTYPE_MAX-1);

	static bool checkBox = true;
	

	ImGui::End();
	ImGui::EndFrame();
	return false;

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CStageImgui::Draw()
{
	CImguiProperty::Draw();
}

//--------------------------------------------------
// block
//--------------------------------------------------
void CStageImgui::ImguiblockPos(D3DXVECTOR3 Pos,D3DXVECTOR3 Log,D3DXVECTOR3 Get,int ModelType)
{

	
	ImGui::Text("number:%d", m_block);
	ImGui::Text("Pos:%.2f,%.2f,%.2f", Pos.x, Pos.y, Pos.z);

	switch ((CMap::BLOCKTYPE)ModelType)
	{
	case CMap::BLOCKTYPE_NORMAL:
		ImGui::Text(u8"普通のやつかなぁ");
		break;
	case CMap::BLOCKTYPE_GOAL:
		ImGui::Text(u8"ゴールだったらいいな");
		break;
	case CMap::BLOCKTYPE_MAX:
		break;
	default:
		break;
	}
	m_block++;
}
