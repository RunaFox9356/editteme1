//==================================================
// game.h
// Author: Buriya Kota
//==================================================
#ifndef _GAME_H_
#define _GAME_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"
#include "model_data.h"

//**************************************************
// 名前付け
//**************************************************
namespace nl = nlohmann;

//**************************************************
// マクロ定義
//**************************************************

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CScore;
class CPause;
class CMeshField;
class CTimer;
class CObjectX;
class CPlayer;
class CStageImgui;
class CMap;
//**************************************************
// クラス
//**************************************************
class CGame : public CGameMode
{
public:
	CGame();
	~CGame() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	// プレイヤーの情報の取得
	static CGame* GetGame() { return m_pGame; }
	CPlayer* GetPlayer3D() { return m_pPlayer; }
	CScore* GetScore() { return m_pScore; }
	CPause* GetPause() { return m_pPause; }
	CMeshField* GetMeshField(int num) { return m_pMeshField[num]; }
	CTimer* GetTimer() { return m_pTimer; }
	static CStageImgui*GetImgui() { return m_imgui; }

	// フレームの設定
	int GetFrame() { return m_time; }

	static CMap*GetMap() { return m_Map; }
	static CGame *Create();

private:
	int m_time;		// ゲーム開始からの時間
	static CGame* m_pGame;
	CPlayer *m_pPlayer;
	CScore *m_pScore;
	CPause *m_pPause;
	std::vector<CMeshField*> m_pMeshField;
	CTimer *m_pTimer;
	static CStageImgui* m_imgui;
	static CMap *m_Map;

};

#endif	// _GAME_H_