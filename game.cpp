//==================================================
// game.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "camera.h"
#include "input.h"
#include "sound.h"
#include "pause.h"
#include "game.h"
#include "ranking.h"
#include "utility.h"

#include "fade.h"
#include "score.h"
#include "timer.h"
#include "silhouette.h"
#include "object3D.h"

#include "model.h"
#include "meshfield.h"
#include "mesh_sky.h"
#include "player.h"
#include "stage_imgui.h"
#include "map.h"
#include "block.h"
//**************************************************
// マクロ定義
//**************************************************
#define MAX_GOAL	(2)

//**************************************************
// 静的メンバ変数
//**************************************************
CGame *CGame::m_pGame = nullptr;
CStageImgui*  CGame::m_imgui = nullptr;
 CMap *CGame::m_Map = nullptr;;
//**************************************************
// マクロ定義
//**************************************************

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGame::CGame()
{
	m_pPlayer = nullptr;
	m_pScore = nullptr;
	m_pPause = nullptr;
	m_pTimer = nullptr;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGame::~CGame()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGame::Init()
{
	m_imgui = new CStageImgui;
	m_imgui->Init(*CManager::GetManager()->GetWnd(), CManager::GetManager()->GetRenderer()->GetDevice());

	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_GAME);
	CManager::GetManager()->GetCamera()->SetParallel(true);
	CManager::GetManager()->GetCamera()->SetPosV(D3DXVECTOR3(0.0f, 200.0f, -1000.0f));
	CManager::GetManager()->GetCamera()->SetPosR(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_time = 0;

	m_pTimer = CTimer::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f));
	m_pTimer->SetTimer(100);

	m_pScore = CScore::Create(D3DXVECTOR3(60.0f, 50.0f, 0.0f), D3DXVECTOR3(40.0f, 100.0f, 0.0f));
	m_pScore->SetScore(0);

	m_pPause = CPause::Create();

	CSilhoette::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),
		D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));

	m_pPlayer = CPlayer::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_Map = CMap::Create();
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGame::Uninit()
{
	// スコアの設定
	CManager::SetNowScore(m_pScore->GetScore());

	m_pPlayer = nullptr;
	m_pTimer = nullptr;
	m_pPause = nullptr;

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGame::Update()
{
	m_time++;
	m_imgui->Update();
#ifdef _DEBUG
	CInput *pInput = CInput::GetKey();

	if (pInput->Trigger(DIK_RETURN) || pInput->Trigger(JOYPAD_B, 0))
	{
		// 遷移
		CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
	}
	if (pInput->Trigger(KEY_MOUSE)&& pInput->Press(KEY_SHIFT))
	{//

		D3DXVECTOR3 BLOCK = pInput->GetMouseCursor();
		BLOCK.y += 200.0f;
	

		BLOCK = m_Map->MapPos(BLOCK);

		CCamera *pCamera = CManager::GetManager()->GetCamera();

		BLOCK = WorldCastScreen(&BLOCK,								// スクリーン座標
			D3DXVECTOR3(CManager::SCREEN_WIDTH, CManager::SCREEN_HEIGHT, 0.0f),			// スクリーンサイズ
			&pCamera->GetViewMatrix(),										// ビューマトリックス
			&pCamera->GetProjMatrix());
		BLOCK.z = 0.0f;
		m_Map->modelset(BLOCK);

		
	}
	if (pInput->Trigger(KEY_SHOT) && pInput->Press(KEY_SHIFT))
	{//

		D3DXVECTOR3 BLOCK = pInput->GetMouseCursor();
		BLOCK.y += 200.0f;

		BLOCK = m_Map->MapPos(BLOCK);

		CCamera *pCamera = CManager::GetManager()->GetCamera();


		BLOCK = WorldCastScreen(&BLOCK,								// スクリーン座標
			D3DXVECTOR3(CManager::SCREEN_WIDTH, CManager::SCREEN_HEIGHT, 0.0f),			// スクリーンサイズ
			&pCamera->GetViewMatrix(),										// ビューマトリックス
			&pCamera->GetProjMatrix());
		BLOCK.z = 0.0f;

		m_Map->blockDes(BLOCK, D3DXVECTOR3(0.0f, 0.0f, 10.0f));
	

	
	}
#endif // _DEBUG
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGame* CGame::Create()
{
	m_pGame = new CGame;

	if (m_pGame != nullptr)
	{
		m_pGame->Init();
	}
	else
	{
		assert(false);
	}

	return m_pGame;
}
