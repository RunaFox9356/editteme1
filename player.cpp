//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"
#include "debug_proc.h"

#include "game.h"

#include "manager.h"
#include "camera.h"
#include "sound.h"
#include "input.h"
#include "model.h"

#include "score.h"
#include "object3D.h"
#include "player.h"
#include "shadow.h"
#include "meshfield.h"
#include "bullet.h"
#include "map.h"

//**************************************************
// マクロ定義
//**************************************************
#define MAX_SPEED			(10.0f)
#define FRICTION			(0.07f)
#define GRAVITY				(4.5f)
#define MAX_ACCEL			(1.0f)
#define MIN_ACCEL			(0.0f)
#define COIN_PARTICLE		(5)
#define NUM_GOAL			(2)
#define KNOCKBACK			(20.0f)

//**************************************************
// 静的メンバ変数
//**************************************************
const float CPlayer::PLAYER_SPEED = 2.0f;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayer::CPlayer(int nPriority) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayer::~CPlayer()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayer::Init()
{

	

	m_nCurrentKey = 0;

	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_posOld = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotDest = D3DXVECTOR3(0.0f, D3DX_PI, 0.0f);
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(50.0f, 50.0f, 50.0f);

	// モデルの生成
	m_pModel[0] = CModel::Create(CModelData::MODEL_PLAYER2_BODY);			// 体
	m_pModel[1] = CModel::Create(CModelData::MODEL_PLAYER2_HEAD);			// 頭
	m_pModel[2] = CModel::Create(CModelData::MODEL_PLAYER2_RIGHT_ARM);		// 右腕
	m_pModel[3] = CModel::Create(CModelData::MODEL_PLAYER2_RIGHT_HAND);		// 右手
	m_pModel[4] = CModel::Create(CModelData::MODEL_PLAYER2_LEFT_ARM);		// 左腕
	m_pModel[5] = CModel::Create(CModelData::MODEL_PLAYER2_LEFT_HAND);		// 左手
	m_pModel[6] = CModel::Create(CModelData::MODEL_PLAYER2_RIGHT_LEG);		// 右足
	m_pModel[7] = CModel::Create(CModelData::MODEL_PLAYER2_RIGHT_FOOT);		// 
	m_pModel[8] = CModel::Create(CModelData::MODEL_PLAYER2_LEFT_LEG);		// 左足
	m_pModel[9] = CModel::Create(CModelData::MODEL_PLAYER2_LEFT_FOOT);		// 
	m_pModel[10] = CModel::Create(CModelData::MODEL_PLAYER2_MANT);			// マント
	m_pModel[11] = CModel::Create(CModelData::MODEL_PLAYER2_SWORD);			// 剣
	m_pModel[12] = CModel::Create(CModelData::MODEL_PLAYER2_COLLISION);		// 当たり判定用モデル

	// 親の設定
	m_pModel[0]->SetParent(nullptr);		// 体
	m_pModel[1]->SetParent(m_pModel[0]);	// 頭
	m_pModel[2]->SetParent(m_pModel[0]);	// 右腕
	m_pModel[3]->SetParent(m_pModel[2]);	// 右手
	m_pModel[4]->SetParent(m_pModel[0]);	// 左腕
	m_pModel[5]->SetParent(m_pModel[4]);	// 左手
	m_pModel[6]->SetParent(m_pModel[0]);	// 右足
	m_pModel[7]->SetParent(m_pModel[6]);	// 
	m_pModel[8]->SetParent(m_pModel[0]);	// 左足
	m_pModel[9]->SetParent(m_pModel[8]);	// 
	m_pModel[10]->SetParent(m_pModel[0]);	// マント
	m_pModel[11]->SetParent(m_pModel[3]);	// 剣
	m_pModel[12]->SetParent(m_pModel[11]);	// 当たり判定用モデル


	LoadSetFile("data/TEXT/motion2.txt");

	for (int nCntModelParts = 0; nCntModelParts < MAX_PARTS; nCntModelParts++)
	{
		m_pModel[nCntModelParts]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
		m_pModel[nCntModelParts]->SetStencil(true);
	}

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CPlayer::Uninit()
{
	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		m_pModel[nCnt]->Uninit();
		delete m_pModel[nCnt];
		m_pModel[nCnt] = nullptr;
	}

	// deleteフラグを立てる
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayer::Update()
{
	// posOld の更新
	D3DXVECTOR3 posOld = GetPos();
	SetPosOld(posOld);

	// 重力
	m_move.y = -GRAVITY;

	if (CGame::GetGame()->GetMap()->blockHit(m_pos, m_size))
	{//ここあたったとき
		m_move.y = 0.0f;
	}
	// モーション
	Motion_();

	// プレイヤーの動き
	Control_();

	SetPos(m_pos);
	SetSize(m_size);

	InScreen_();

	D3DXVECTOR3 pos = GetPos();
	pos += m_move;
	SetPos(pos);

#ifdef _DEBUG
	// デバッグ表示
	CDebugProc::Print("プレイヤーの現在の角度 : %f\n", m_rot.y);
	CDebugProc::Print("プレイヤーの現在の位置 : %f,%f,%f\n", m_pos.x, m_pos.y, m_pos.z);
	CDebugProc::Print("プレイヤーの過去の位置 : %f,%f,%f\n", m_posOld.x, m_posOld.y, m_posOld.z);
	CDebugProc::Print("プレイヤーのMOVE : %+f, %+f, %+f\n", m_move.x, m_move.y, m_move.z);
#endif // DEBUG
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CPlayer::Draw(DRAW_MODE drawMode)
{
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans;
	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtx);

	// 向きを反映							↓rotの情報を使って回転行列を作る
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtx, &m_mtx, &mtxRot);		// 行列掛け算関数		第二引数 * 第三引数 を　第一引数に格納

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtx, &m_mtx, &mtxTrans);

	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		m_pModel[nCnt]->Draw(&m_mtx, drawMode);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayer* CPlayer::Create(D3DXVECTOR3 pos)
{
	CPlayer *pPlayer3D = nullptr;
	pPlayer3D = new CPlayer;

	if (pPlayer3D != nullptr)
	{
		pPlayer3D->Init();
		pPlayer3D->SetPos(pos);
	}
	else
	{
		assert(false);
	}

	return pPlayer3D;
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}

//--------------------------------------------------
// 画面内の納める
//--------------------------------------------------
void CPlayer::InScreen_()
{
	D3DXVECTOR3 pos = GetPos();

	// 画面内に収める処理
	if (pos.y < -240.0f)
	{// 上
		pos.y = -240.0f;
		m_move.y = 0.0f;
	}
	if (pos.x > 360.0f)
	{// 右
		pos.x = 360.0f;
	}
	if (pos.x < -360.0f)
	{// 左
		pos.x = -360.0f;
	}

	SetPos(pos);
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CPlayer::Control_()
{
	// インプット
	CInput *pInput = CInput::GetKey();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	D3DXVECTOR3 rotDest = GetRotDest();

	if ((vec.x == 0.0f) && (vec.y == 0.0f))
	{
		if (pInput->Press(KEY_LEFT))
		{// 左
			vec.x += -1.0f;
		}
		if (pInput->Press(KEY_RIGHT))
		{// 右
			vec.x += 1.0f;
		}
		if (pInput->Press(KEY_UP))
		{// 上
			vec.y += 1.0f;
		}
		if (pInput->Press(KEY_DOWN))
		{// 下
			vec.y += -1.0f;
		}
	}

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();

	if (vec.x != 0.0f || vec.y != 0.0f)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		// キーボード
		pos += vec * PLAYER_SPEED;
		SetPos(pos);
	}

	if (pInput->Trigger(KEY_SHOT))
	{// 弾発射
		CBullet::Create(pos, D3DXVECTOR3(10.0f, 10.0f, 0.0f));
	}
}

//--------------------------------------------------
// モーション
//--------------------------------------------------
void CPlayer::Motion_()
{
	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{// モデルに情報がなければ
			continue;
		}

		int nNextNumber = m_nCurrentKey + 1;

		if (nNextNumber >= m_ModelData[0].NUM_KEY)
		{
			nNextNumber = 0;
		}


		// 差分
		D3DXVECTOR3 difPos = m_ModelData[0].KeySet[nNextNumber].aKey[nCnt].pos - m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].pos;
		D3DXVECTOR3 difRot = m_ModelData[0].KeySet[nNextNumber].aKey[nCnt].rot - m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].rot;

		// 相対値
		float nRelativeValue = (float)m_nCountMotion / (float)m_ModelData[0].KeySet[m_nCurrentKey].nFrame;

		// 現在地
		D3DXVECTOR3 currentPos = m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].pos + (difPos * nRelativeValue)+ m_pModel[nCnt]->GetPos();
		D3DXVECTOR3 currentRot = m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].rot + (difRot * nRelativeValue);

		m_pModel[nCnt]->SetPos(currentPos);
		m_pModel[nCnt]->SetRot(currentRot);
	}

	// モーションカウンターを進める
	m_nCountMotion++;

	if (m_nCountMotion >= m_ModelData[0].KeySet[m_nCurrentKey].nFrame)
	{
		m_nCurrentKey++;
		m_nCountMotion = 0;

		if (m_nCurrentKey >= m_nNumKey)
		{
			m_nCurrentKey = 0;
		}
	}
}


//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CPlayer::LoadSetFile(char *Filename)
{
	m_nSetCurrentMotion = 0;
	char modelFile[256];
	char String[256];
	// ファイルポインタの宣言
	FILE* pFile;

	// ファイルを開く
	pFile = fopen(Filename, "r");

	if (pFile != NULL)
	{// ファイルが開いた場合
		fscanf(pFile, "%s", &String);

		while (strncmp(&String[0], "SCRIPT", 6) != 0)
		{// スタート来るまで空白読み込む
			String[0] = {};
			fscanf(pFile, "%s", &String[0]);
		}
		D3DXVECTOR3	s_modelMainpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		while (strncmp(&String[0], "END_SCRIPT", 10) != 0)
		{// 文字列の初期化と読み込み// 文字列の初期化と読み込み
			fscanf(pFile, "%s", &String[0]);

			if (strcmp(&String[0], "MODEL_FILENAME") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &modelFile);
			}
			if (strcmp(&String[0], "MAINPOS") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
				fscanf(pFile, "%f", &s_modelMainpos.x);
				fscanf(pFile, "%f", &s_modelMainpos.y);
				fscanf(pFile, "%f", &s_modelMainpos.z);
			}
			else if (strcmp(&String[0], "CHARACTERSET") == 0)
			{
				while (1)
				{
					int Index = 0;
					fscanf(pFile, "%s", &String[0]);
		
					if (strcmp(&String[0], "PARTSSET") == 0)
					{
						while (1)
						{// 文字列の初期化と読み込み// 文字列の初期化と読み込み
							fscanf(pFile, "%s", &String[0]);
							if (strcmp(&String[0], "INDEX") == 0)
							{// 文字列が一致した場合
								fscanf(pFile, "%s", &String[0]);
								fscanf(pFile, "%d", &Index);
							}
							if (strcmp(&String[0], "POS") == 0)
							{
								D3DXVECTOR3 Pos = { 0.0f,0.0f,0.0f };
								fscanf(pFile, "%s", &String[0]);
								fscanf(pFile, "%f", &Pos.x);
								fscanf(pFile, "%f", &Pos.y);
								fscanf(pFile, "%f", &Pos.z);
								m_pModel[Index]->SetPos(Pos);
							}
							if (strcmp(&String[0], "ROT") == 0)
							{
								D3DXVECTOR3 Rot = { 0.0f,0.0f,0.0f };
								fscanf(pFile, "%s", &String[0]);
								fscanf(pFile, "%f", &Rot.x);
								fscanf(pFile, "%f", &Rot.y);
								fscanf(pFile, "%f", &Rot.z);
								m_pModel[Index]->SetRot(Rot);
							}
							if (strcmp(&String[0], "END_PARTSSET") == 0)
							{
								break;
							}
						}
					}
					if (strcmp(&String[0], "END_CHARACTERSET") == 0)
					{
						break;
					}
				}

			}
			else if ((strcmp(&String[0], "MODELSET") == 0) || (strcmp(&String[0], "MOTIONSET") == 0))
			{// 文字列が一致した場合
				D3DXVECTOR3	s_modelpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3	s_modelrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

				while (1)
				{
					// 文字列の初期化と読み込み
					String[0] = {};
					fscanf(pFile, "%s", &String[0]);

					if (strncmp(&String[0], "#", 1) == 0)
					{// これのあとコメント
						fgets(&String[0], sizeof(String), pFile);
						continue;
					}

					if (strcmp(&String[0], "POS") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%f", &s_modelpos.x);
						fscanf(pFile, "%f", &s_modelpos.y);
						fscanf(pFile, "%f", &s_modelpos.z);
					}

					if (strcmp(&String[0], "ROT") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%f", &s_modelrot.x);
						fscanf(pFile, "%f", &s_modelrot.y);
						fscanf(pFile, "%f", &s_modelrot.z);
					}

					if (strcmp(&String[0], "LOOP") == 0)
					{// 文字列が一致した場合//ループするかしないか1する０しない
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].LOOP);
					}

					if (strcmp(&String[0], "NUM_KEY") == 0)
					{// 文字列が一致した場合//キーの最大数
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].NUM_KEY);
					}

					if (strcmp(&String[0], "KEYSET") == 0)
					{// 文字列が一致した場合//アニメーションのファイルだったとき
						LoadKeySetFile(pFile);
					}

					if (strcmp(&String[0], "END_MOTIONSET") == 0)
					{// 一回のmotion読み込み切ったら次のmotionのセットに行くためにカウント初期化してデータを加算する
						m_nSetModel = 0;
						m_nSetCurrentMotion++;
					}

					if (strcmp(&String[0], "END_SCRIPT") == 0)
					{// 文字列が一致した場合
						break;
					}
				}
			}
		}
	}

	//ファイルを閉じる
	fclose(pFile);
}

//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CPlayer::LoadKeySetFile(FILE * pFile)
{
	char String[256];
	int nSetKey = 0;

	while (1)
	{
		// 文字列の初期化と読み込み
		fscanf(pFile, "%s", &String[0]);

		if (strncmp(&String[0], "#", 1) == 0)
		{//コメント対策
			fgets(&String[0], sizeof(String), pFile);
			continue;
		}

		if (strcmp(&String[0], "FRAME") == 0)
		{// 文字列が一致した場合
			fscanf(pFile, "%s", &String[0]);	// イコール読み込む
			fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].nFrame);
		}

		if (strcmp(&String[0], "KEY") == 0)
		{
			while (1)
			{// 文字列の初期化と読み込み
				String[0] = {};
				fscanf(pFile, "%s", &String[0]);
				if (strncmp(&String[0], "#", 1) == 0)
				{
					fgets(&String[0], sizeof(String), pFile);		// コメント読み込む
					continue;
				}

				if (strcmp(&String[0], "POS") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);	//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.z);
				}

				if (strcmp(&String[0], "ROT") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);	//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.z);
				}

				if (strcmp(&String[0], "END_KEY") == 0)
				{
					nSetKey++;//パーツの数がどんどん増える
					break;
				}
			}
		}

		if (strcmp(&String[0], "END_KEYSET") == 0)
		{// 文字列が一致した場合
			m_nSetModel++;		// 現在のセットしてる番号の更新
			nSetKey = 0;		// パーツの数初期化
			break;
		}
	}
}
