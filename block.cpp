//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "block.h"
#include "model_data.h"
#include "input.h"
#include "utility.h"
#include "camera.h"
#include "debug_proc.h"
#include "game.h"
#include "player.h"
#include "map.h"
#include "fade.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBlock::CBlock(int nPriority /* =6 */) : CObjectX(nPriority)
{
	m_MovePos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBlock::~CBlock()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBlock::Init()
{
	CObjectX::Init();

	CObjectX::SetModelData(CModelData::MODEL_BOX);
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CBlock::Uninit()
{
	CObjectX::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBlock::Update()
{
	CObjectX::Update();
	CInput *pInput = CInput::GetKey();
	int wheel = pInput->GetMouseWheel();

	
	if (pInput->Press(KEY_DOWN))
	{
		//m_MovePos.y += BLOCKSIZE;
		D3DXVECTOR3 Pos = GetPos();
		Pos.y  -= BLOCKSIZE;


		SetPos(D3DXVECTOR3(Pos));
	}
	if (pInput->Press(KEY_UP))
	{
		//m_MovePos.y -= BLOCKSIZE;
		D3DXVECTOR3 Pos = GetPos();
		Pos.y += BLOCKSIZE;
		SetPos(D3DXVECTOR3(Pos));
	}
	
	if (wheel > 0)
	{
	
		D3DXVECTOR3 Pos = GetPos();
		Pos.y = GetPos().y + BLOCKSIZE;
		SetPos(D3DXVECTOR3(Pos));
	}
	else if (wheel < 0)
	{
		
		D3DXVECTOR3 Pos = GetPos();
		Pos.y = GetPos().y - BLOCKSIZE;
		SetPos(D3DXVECTOR3(Pos));
	}
	


}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CBlock::Draw(DRAW_MODE drawMode)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	CObjectX::Draw(drawMode);
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CBlock * CBlock::Create(D3DXVECTOR3 pos, int Model)
{
	CBlock *pObjectX;
	pObjectX = new CBlock;

	if (pObjectX != nullptr)
	{
		pObjectX->Init();
		pObjectX->SetPos(pos);
		pObjectX->SetPosOriginPos(pos);
		pObjectX->SetModelType(Model);
	}
	else
	{
		assert(false);
	}

	return pObjectX;

}

//=============================================================================
// 当たり判定
//=============================================================================
bool CBlock::Collision(const D3DXVECTOR3 PlayerPos, const D3DXVECTOR3 PlayerSize)
{
	CModelData* modelData = CManager::GetManager()->GetModelData();
	
	D3DXVECTOR3 Size = modelData->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();


	if (((Pos.y - Size.y * 0.5f) <= (PlayerPos.y + PlayerSize.y)) &&
		((Pos.y + Size.y * 0.5f) >= (PlayerPos.y - PlayerSize.y)) &&
		((Pos.x - Size.x * 0.5f) <= (PlayerPos.x + PlayerSize.x)) &&
		((Pos.x + Size.x * 0.5f) >= (PlayerPos.x - PlayerSize.x)))
	{
		hitevent();
		return true;
	}
	return false;
}

//=============================================================================
// ヒット
//=============================================================================
void CBlock::hitevent()
{


}

