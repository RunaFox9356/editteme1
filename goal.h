//==================================================
// goal.h
// Author: Buriya Kota
//==================================================
#ifndef _GOAL_H_
#define _GOAL_H_

//**************************************************
// インクルード
//**************************************************
#include "block.h"

//**************************************************
// クラス
//**************************************************
class CGoal :public CBlock
{
public:
	explicit CGoal(int nPriority = PRIORITY_OBJECT);
	~CGoal();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;
	static CGoal *CGoal::Create(D3DXVECTOR3 pos, int Model);
	void hitevent()override;
	
private:

};

#endif	// _GOAL_H_