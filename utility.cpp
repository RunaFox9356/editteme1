//==================================================
// utility.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "utility.h"
#include "manager.h"
#include "camera.h"


namespace hmd
{
	D3DXMATRIX *giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard);
	D3DXMATRIX *giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard);
	float easeInSine(float X);
	float easeInQuad(float X);
	bool is_sjis_lead_byte(int c);
}

//--------------------------------------------------
// ワールド座標からスクリーン座標に変更する関数
//--------------------------------------------------
D3DXVECTOR3 GetWorldToScreenPos(const D3DXVECTOR3& pos)
{
	// https://dxlib.xsrv.jp/cgi/patiobbs/patio.cgi?mode=view&no=3145

	D3DXMATRIX mtx;

	// 位置を反映
	D3DXMatrixTranslation(&mtx, pos.x, pos.y, pos.z);

	// ビュー変換とプロジェクション変換
	mtx = mtx * CManager::GetManager()->GetCamera()->GetViewMatrix() * CManager::GetManager()->GetCamera()->GetProjMatrix();

	// 座標を取得
	D3DXVECTOR3 pOut(mtx.m[3]);

	// zで割って-1~1の範囲に収める
	pOut /= pOut.z;

	// スクリーン行列
	D3DXMATRIX viewport;
	D3DXMatrixIdentity(&viewport);
	viewport._11 = CManager::SCREEN_WIDTH / 2.0f; 
	viewport._22 = -CManager::SCREEN_HEIGHT / 2.0f;
	viewport._41 = CManager::SCREEN_WIDTH / 2.0f;
	viewport._42 = CManager::SCREEN_HEIGHT / 2.0f;

	// スクリーン変換
	D3DXVec3TransformCoord(&pOut, &pOut, &viewport);

	return pOut;

#if 0
	// http://yamatyuu.net/computer/program/directx9/3dview/index.html

	D3DVIEWPORT9 viewport;
	viewport.X = 0.0f;
	viewport.Y = 0.0f;

	// ビューポートの幅
	viewport.Width = CManager::SCREEN_WIDTH;
	// ビューポートの高さ
	viewport.Height = CManager::SCREEN_HEIGHT;
	// ビューポート深度設定
	viewport.MinZ = 0.0f;
	viewport.MaxZ = 1.0f;

	D3DXMATRIX view, proj, mtxWorld;

	// ビュー行列と射影行列の取得
	view = CManager::GetManager()->GetCamera()->GetViewMatrix();
	proj = CManager::GetManager()->GetCamera()->GetProjMatrix();
	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&mtxWorld);

	D3DXVECTOR3 pOut;
	D3DXVec3Project(&pOut, &pos, &viewport, &proj, &view, &mtxWorld);

	return pOut;
#endif
}

//--------------------------------------------------
// 2Dベクトルの外積
//--------------------------------------------------
float Vec2Cross(D3DXVECTOR3* v1, D3DXVECTOR3* v2)
{
	return v1->x * v2->z - v1->z * v2->x;
}

//--------------------------------------------------
// 2Dベクトルの内積
//--------------------------------------------------
float D3DXVec2Dot(D3DXVECTOR3* v1, D3DXVECTOR3* v2)
{
	return v1->x * v2->x + v1->z * v2->z;
}

//---------------------------------------------------------------------------
// 小数点のランダム
//---------------------------------------------------------------------------
float FloatRandam(float fMax, float fMin)
{
	return ((rand() / (float)RAND_MAX) * (fMax - fMin)) + fMin;
}


///=============================================================================
//マトリックスを回転させるやつ
//=============================================================================
D3DXMATRIX *hmd::giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard)
{
	// TODO: 関数化する
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxView;

	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(pOut);


	if (Billboard)
	{
		LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();

		pDevice->GetTransform(D3DTS_VIEW, &mtxView);
		//Ｚ軸で回転しますちなみにm_rotつかうとグルグル回ります
		pOut->_11 = mtxView._11;
		pOut->_12 = mtxView._21;
		pOut->_13 = mtxView._31;
		pOut->_31 = mtxView._13;
		pOut->_32 = mtxView._23;
		pOut->_33 = mtxView._33;

	}

	D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.x, rot.y, rot.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(pOut, pOut, &mtxRot);

	// 位置を反映
	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(pOut, pOut, &mtxTrans);
	return pOut;
}

///=============================================================================
//マトリックスを回転させるやつ
//=============================================================================
D3DXMATRIX *hmd::giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard)
{
	// TODO: 関数化する
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxView;

	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(pOut);


	if (Billboard)
	{
		LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();

		pDevice->GetTransform(D3DTS_VIEW, &mtxView);
		//Ｚ軸で回転しますちなみにm_rotつかうとグルグル回ります
		pOut->_11 = mtxView._11;
		pOut->_12 = mtxView._21;
		pOut->_13 = mtxView._31;
		pOut->_31 = mtxView._13;
		pOut->_32 = mtxView._23;
		pOut->_33 = mtxView._33;

	}

	// クォータニオンの使用した姿勢の設定
	D3DXMatrixRotationQuaternion(&mtxRot, &Quat);            // クオータニオンによる行列回転
	D3DXMatrixMultiply(pOut, pOut, &mtxRot);    // 行列掛け算関数(第2引数×第3引数第を１引数に格納)

												// 位置を反映
												// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(pOut, pOut, &mtxTrans);
	return pOut;
}

//
//イージング
//
float hmd::easeInSine(float X)
{
	return 1 - cos((X * D3DX_PI) / 2);
}

float hmd::easeInQuad(float X)
{
	return X * X;
}

//=============================================================================
//1バイト文字をシフトJISかどうか判定する関数
//=============================================================================
bool hmd::is_sjis_lead_byte(int c)
{
	return (((c & 0xffu) ^ 0x20u) - 0xa1) < 94u / 2;
}


//---------------------------------------------------------------------------
// スクリーン座標をワールド座標へのキャスト
//---------------------------------------------------------------------------
D3DXVECTOR3 WorldCastScreen(D3DXVECTOR3 *screenPos,			// スクリーン座標
	D3DXVECTOR3 screenSize,									// スクリーンサイズ
	D3DXMATRIX* mtxView,									// ビューマトリックス
	D3DXMATRIX* mtxProjection)								// プロジェクションマトリックス
{
	// 変数宣言
	D3DXVECTOR3 ScreenPos;

	// 計算用マトリックスの宣言
	D3DXMATRIX InvView, InvPrj, VP, InvViewport;

	// 各行列の逆行列を算出
	D3DXMatrixInverse(&InvView, NULL, mtxView);
	D3DXMatrixInverse(&InvPrj, NULL, mtxProjection);
	D3DXMatrixIdentity(&VP);
	VP._11 = screenSize.x / 2.0f; VP._22 = -screenSize.y / 2.0f;
	VP._41 = screenSize.x / 2.0f; VP._42 = screenSize.y / 2.0f;
	D3DXMatrixInverse(&InvViewport, NULL, &VP);

	// ワールド座標へのキャスト
	D3DXMATRIX mtxWorld = InvViewport * InvPrj * InvView;
	D3DXVec3TransformCoord(&ScreenPos, screenPos, &mtxWorld);

	return ScreenPos;
}