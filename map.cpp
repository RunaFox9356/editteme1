//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "object2D.h"
#include "renderer.h"
#include "map.h"
#include "mapblack.h"
#include "game.h"
#include "stage_imgui.h"
#include "goal.h"
#include "input.h"
#include "item.h"
//**************************************************
// 定数定義
//**************************************************
namespace
{
	// ポリゴンの幅
	const float POLYGON_WIDTH = 50.0f;
	// ポリゴンの幅
	const float POLYGON_HEIGHT = 50.0f;
}

// 形成する四角形の基準値
const D3DXVECTOR3 sVtx[4] =
{
	D3DXVECTOR3(-1.0f, -1.0f, 0.0f),
	D3DXVECTOR3(+1.0f, -1.0f, 0.0f),
	D3DXVECTOR3(-1.0f, +1.0f, 0.0f),
	D3DXVECTOR3(+1.0f, +1.0f, 0.0f),
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMap::CMap(int nPriority) : CObject(nPriority)
{
	for (int X = 0; X < MaxX; X++)
	{
		for (int Y = 0; Y < MaxY; Y++)
		{
		
			m_map[X][Y] = nullptr;
			
		}
	}
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMap::~CMap()
{
	
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMap::Init()
{
	m_Log = 0;
	D3DXVECTOR3 ScreenPos = D3DXVECTOR3(CManager::SCREEN_WIDTH, CManager::SCREEN_HEIGHT, 0.0f);
	for (int X = 0; X < MaxX; X++)
	{
		for (int Y = 0; Y < MaxY; Y++)
		{
			if (m_map[X][Y] == nullptr)
			{
				m_map[X][Y] = CMapBlack::Create(D3DXVECTOR3(BLOCKSIZE*(X), BLOCKSIZE*(Y),-0.0f), D3DXVECTOR3(BLOCKSIZE, BLOCKSIZE, 0.0f), PRIORITY_UI);
				
			}
		}

	}
	

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMap::Uninit()
{

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMap::Update()
{
	CInput *pInput = CInput::GetKey();
	int wheel = pInput->GetMouseWheel();


	if (pInput->Press(KEY_DOWN))
	{
		m_Log -= BLOCKMOVE;
	
	}
	if (pInput->Press(KEY_UP))
	{
		m_Log += BLOCKMOVE;
	
	}


	if (wheel > 0)
	{
		m_Log += BLOCKMOVE;

	}
	else if (wheel < 0)
	{
		m_Log -= BLOCKMOVE;
	
	}

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMap::Draw(DRAW_MODE /*drawMode*/)
{
	
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CMap *CMap::Create()
{
	CMap *pObject2D;
	pObject2D = new CMap(0);

	if (pObject2D != nullptr)
	{
		pObject2D->Init();
		
	}
	else
	{
		assert(false);
	}

	return pObject2D;
}

//--------------------------------------------------
// map
//--------------------------------------------------
D3DXVECTOR3 CMap::MapPos(const D3DXVECTOR3 pos)
{
	
	D3DXVECTOR3 mapPos;
	D3DXVECTOR3 Pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	for (int X = 0; X < MaxX; X++)
	{
		for (int Y = 0; Y < MaxY; Y++)
		{
			mapPos = m_map[X][Y]->GetPos();

			if (((mapPos.x- m_map[X][Y]->GetSize().x/2  < pos.x) && (mapPos.x + m_map[X][Y]->GetSize().x / 2  > pos.x)) &&
				((mapPos.y < pos.y) && (mapPos.y + m_map[X][Y]->GetSize().y > pos.y)))
			{
				Pos.x = mapPos.x;
				Pos.y = mapPos.y +m_map[X][Y]->GetSize().y;
			}
		}
	}
	return Pos;
}

//--------------------------------------------------
// block召喚
//--------------------------------------------------
void CMap::modelset(const D3DXVECTOR3 pos)
{
	int Molde = CGame::GetImgui()->GetModel();
	switch ((BLOCKTYPE)Molde)
	{
	case CMap::BLOCKTYPE_NORMAL:
		Addblock(CBlock::Create(pos, Molde));
		break;
	case CMap::BLOCKTYPE_GOAL:
		//Addblock(CBlock::Create(pos, Molde));
		Addblock(CGoal::Create(pos, Molde));
		break;
	case CMap::BLOCKTYPE_ITEM:
		Addblock(CItem::Create(pos, Molde));
		break;
	case CMap::BLOCKTYPE_MAX:
		break;
	default:
		break;
	}
	//Addblock(CBlock::Create(pos, 2));
}

//--------------------------------------------------
// セーブ
//--------------------------------------------------
void CMap::Save()
{
	int nIndex = 0;
	std::string MyfilePass = ("data/TEXT/test.json");

	for (int nCnt = 0; nCnt < GetblockSize(); nCnt++)
	{

		std::string name = "BLOCK";
		std::string Number = std::to_string(nIndex);
		name += Number;
		m_Jsonblock[name] = {{ "POS",{
			{ "X", Getblock(nCnt)->GetPos().x } ,
			{ "Y", Getblock(nCnt)->GetPos().y - m_Log } ,
			{ "Z", Getblock(nCnt)->GetPos().z } } },
			{ "TYPE", Getblock(nCnt)->GetModelType() }
	};
	
		nIndex++;
	}
	m_Jsonblock["INDEX"] = nIndex;

	auto jobj = m_Jsonblock.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = MyfilePass.c_str();
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();

}

//--------------------------------------------------
// ロード
//--------------------------------------------------
void CMap::Load()
{

	std::string MyfilePass = ("data/TEXT/test.json");
	std::ifstream ifs(MyfilePass.c_str());

	int nIndex = 0;

	if (ifs)
	{
		ifs >> m_Jsonblock;
		nIndex = m_Jsonblock["INDEX"];


		for (int nCnt = 0; nCnt < nIndex; nCnt++)
		{
			std::string name = "BLOCK";
			std::string Number = std::to_string(nCnt);
			name += Number;

		
			D3DXVECTOR3 Pos = (D3DXVECTOR3(m_Jsonblock[name]["POS"]["X"], m_Jsonblock[name]["POS"]["Y"], m_Jsonblock[name]["POS"]["Z"]));
			int Type = m_Jsonblock[name]["TYPE"];

			switch ((BLOCKTYPE)Type)
			{
			case CMap::BLOCKTYPE_NORMAL:
				Addblock(CBlock::Create(Pos, Type));
				break;
			case CMap::BLOCKTYPE_GOAL:
				//Addblock(CBlock::Create(Pos, Type));
				Addblock(CGoal::Create(Pos, Type));
				break;
			case CMap::BLOCKTYPE_ITEM:
				Addblock(CItem::Create(Pos, Type));
				break;
			case CMap::BLOCKTYPE_MAX:
				break;
			default:
				break;
			}
			
		}
	}
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
bool CMap::blockHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size)
{
	D3DXVECTOR3 ScreenPos = D3DXVECTOR3(CManager::SCREEN_WIDTH, CManager::SCREEN_HEIGHT, 0.0f);
	bool Is = false;
	for (int block = 0; block < GetblockSize(); block++)
	{

		CBlock* Noublock = Getblock(block);
		D3DXVECTOR3 blockPos = Noublock->GetPos();
		D3DXVECTOR3 blockSize = Noublock->GetSize();

		if (((blockPos.y - blockSize.y) <= (ScreenPos.y)) &&
			((blockPos.y + blockSize.y) >= (-ScreenPos.y)) &&
			((blockPos.x - blockSize.x) <= (ScreenPos.x)) &&
			((blockPos.x + blockSize.x) >= (-ScreenPos.x)))
		{
			Is = Noublock->Collision(pos, Size);
			if (Is)
			{
				return Is;
			}
			
		}
	}
	return Is;
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
bool CMap::blockDes(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size)
{
	D3DXVECTOR3 ScreenPos = D3DXVECTOR3(CManager::SCREEN_WIDTH, CManager::SCREEN_HEIGHT, 0.0f);
	bool Is = false;
	for (int block = 0; block < GetblockSize(); block++)
	{

		CBlock* Noublock = Getblock(block);
		D3DXVECTOR3 blockPos = Noublock->GetPos();
		D3DXVECTOR3 blockSize = Noublock->GetSize();

		if (((blockPos.y - blockSize.y) <= (ScreenPos.y)) &&
			((blockPos.y + blockSize.y) >= (-ScreenPos.y)) &&
			((blockPos.x - blockSize.x) <= (ScreenPos.x)) &&
			((blockPos.x + blockSize.x) >= (-ScreenPos.x)))
		{
			Is = Noublock->Collision(pos, Size);
			if (Is)
			{
				Noublock->Uninit();
				blockListdes(block);
				return Is;
			}

		}
	}
	return Is;
}






