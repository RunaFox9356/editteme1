//==================================================
// goal.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "item.h"

#include "model_data.h"
#include "fade.h"
#include "game.h"
#include "manager.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CItem::CItem(int nPriority) : CBlock(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CItem::~CItem()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CItem::Init()
{
	CBlock::Init();

	CObjectX::SetModelData(CModelData::MODEL_COIN);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CItem::Uninit()
{
	CBlock::Uninit();
}


//--------------------------------------------------
// 更新
//--------------------------------------------------
void CItem::Update()
{
	CBlock::Update();

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CItem::Draw(DRAW_MODE drawMode)
{

	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	CBlock::Draw(drawMode);
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CItem *CItem::Create(D3DXVECTOR3 pos, int Model)
{
	CItem *pItem;
	pItem = new CItem;

	if (pItem != nullptr)
	{
		pItem->Init();
		pItem->SetPos(pos);
		pItem->SetPosOriginPos(pos);
		pItem->SetModelType(Model);
	}
	else
	{
		assert(false);
	}

	return pItem;
}


//=============================================================================
// ヒット
//=============================================================================
void CItem::hitevent()
{
	//CFade::GetInstance()->SetFade(CManager::MODE_GAME);
}