//============================
//
// マップチップヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MAP_H_
#define _MAP_H_

//**************************************************
// インクルード
//**************************************************
#include "main.h"
#include "texture.h"
#include "object.h"
#include "block.h"

class CMapBlack;
namespace nl = nlohmann;

//**************************************************
// クラス
//**************************************************
class CMap : public CObject
{
public:
	const float BLOCKMOVE = 5.0f;
	enum BLOCKTYPE
	{
		BLOCKTYPE_NORMAL = 0,
		BLOCKTYPE_GOAL,
		BLOCKTYPE_ITEM,
		BLOCKTYPE_MAX
	};
	static const int MaxX = 30;
	static const int MaxY = 20;

	const float BLOCKSIZE = 50.0f;
	explicit CMap(int nPriority = PRIORITY_OBJECT);
	~CMap() override;

	virtual HRESULT Init() override;
	virtual void Uninit() override;
	virtual void Update() override;
	virtual void Draw(DRAW_MODE drawMode) override;
	static CMap *CMap::Create();
	D3DXVECTOR3 MapPos(const D3DXVECTOR3 pos);
	void modelset(const D3DXVECTOR3 pos);

	//	blockポイントを追加
	void Addblock(const CBlock *block) { m_block.push_back((CBlock*)block); }
	//	blockポイントをセット
	void Setblock(const int IsPoptime, const CBlock *block) { m_block.at(IsPoptime) = (CBlock*)block; }
	//	blockポイントをゲット
	CBlock* Getblock(const int IsPoptime) { return m_block.at(IsPoptime); }
	//	blockポイントのさいずの取得
	int GetblockSize() { return m_block.size(); }
	
	//	blockポイントをゲット
	void blockListdes(const int IsPoptime) { m_block.erase(m_block.begin() + IsPoptime); }

	float GetLog() { return m_Log; }



	void Save();
	void Load();
	bool blockHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size);
	bool blockDes(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size);
private:

	CMapBlack*m_map[MaxX][MaxY];
	std::vector<CBlock*> m_block;
	nl::json m_Jsonblock;//リストの生成
	float m_Log;
};

#endif	// _OBJECT2D_H_