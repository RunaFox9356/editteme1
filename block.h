//============================
//
// 障害物
// Author:hamada ryuuga
//
//============================
#ifndef _BLOCK_H_
#define _BLOCK_H_

//**************************************************
// インクルード
//**************************************************
#include "objectX.h"


//**************************************************
// クラス
//**************************************************
class CBlock :public CObjectX
{
private:


public:

	const float BLOCKSIZE = 50.0f;

	explicit CBlock(int nPriority = PRIORITY_OBJECT);
	~CBlock();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;
	virtual void hitevent();
	static CBlock *CBlock::Create(D3DXVECTOR3 pos, int ModelType);

	D3DXVECTOR3 GetPosOriginPos() { return m_PosOrigin; }
	void SetPosOriginPos(D3DXVECTOR3 Pos) { m_PosOrigin = Pos; }

	D3DXVECTOR3 GetNouPos() { return GetPos() -m_MovePos; }
	D3DXVECTOR3 GetMovePos() { return m_MovePos; }


	int GetModelType() { return m_ModelType; }
	void SetModelType(int Model) { m_ModelType = Model; }

	bool Collision(const D3DXVECTOR3 PlayerPos, const D3DXVECTOR3 PlayerSize);


	
private:


	D3DXVECTOR3 m_MovePos;
	D3DXVECTOR3 m_PosOrigin;
	int m_ModelType;
};

#endif	// _FADE_H_#pragma once
