//==================================================
// item.h
// Author: Buriya Kota
//==================================================
#ifndef _ITEM_H_
#define _ITEM_H_

//**************************************************
// インクルード
//**************************************************
#include "block.h"

//**************************************************
// クラス
//**************************************************
class CItem :public CBlock
{
public:
	explicit CItem(int nPriority = PRIORITY_OBJECT);
	~CItem();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;
	static CItem *CItem::Create(D3DXVECTOR3 pos, int Model);
	void hitevent()override;

private:

};

#endif	// _GOAL_H_#pragma once
