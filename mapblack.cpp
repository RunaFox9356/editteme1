//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "mapblack.h"



//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMapBlack::CMapBlack(int nPriority /* =6 */) : CObject2D(nPriority)
{
	
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMapBlack::~CMapBlack()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMapBlack::Init()
{
	CObject2D::Init();

	CObject2D::SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMapBlack::Uninit()
{

	CObject2D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMapBlack::Update()
{
	CObject2D::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMapBlack::Draw(DRAW_MODE drawMode)
{


	CObject2D::Draw(drawMode);
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CMapBlack * CMapBlack::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int nPriority)
{

	CMapBlack *pObject2D;
	pObject2D = new CMapBlack(nPriority);

	if (pObject2D != nullptr)
	{
		pObject2D->Init();
		pObject2D->SetPos(pos);
		pObject2D->SetSize(size);
	
	}
	else
	{
		assert(false);
	}

	return pObject2D;

}
